#pragma once

#include "ofMain.h"
//#include "Ribbon.h"

class Ribbon;

class RibbonParticle : public ofBaseApp {

public:
	float px, py;                                       // x and y position of particle (this is the bexier point)
	float xSpeed, ySpeed;                           // speed of the x and y positions
	float cx1, cy1, cx2, cy2;                           // the avarage x and y positions between px and py and the points of the surrounding Particles
	float leftPX, leftPY, rightPX, rightPY;             // the x and y points of that determine the thickness of this segment
	float lpx, lpy, rpx, rpy;                           // the x and y points of the outer bezier points
	float lcx1, lcy1, lcx2, lcy2;                       // the avarage x and y positions between leftPX and leftPX and the left points of the surrounding Particles
	float rcx1, rcy1, rcx2, rcy2;                       // the avarage x and y positions between rightPX and rightPX and the right points of the surrounding Particles
	float radius;                                       // thickness of current particle
	float randomness;
	Ribbon *ribbon;

	RibbonParticle();
	RibbonParticle(float rand, Ribbon *r);
	void setup();
	void update();
	void draw();

	void calculateParticles(const RibbonParticle& pMinus1, const RibbonParticle& pPlus1, int particleMax, int i);
};