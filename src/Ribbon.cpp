#include "Ribbon.h"

Ribbon::Ribbon(){

}

Ribbon::Ribbon(int _ribbonParticleAmount, ofColor _ribbonColor, float _randomness){
	ribbonParticleAmount = _ribbonParticleAmount;
	ribbonColor = _ribbonColor;
	randomness = _randomness;

	//ofEnableSmoothing();

	ribbonAmount = 1;
	particlesAssigned = 0;
	radiusMax = 8;        
	radiusDivide = 10;    
	gravity = .03;        
	friction = 1.1;       
	maxDistance = 40;     
	drag = 2;             
	dragFlare = .008; 
}

void Ribbon::setup(){
	
}

void Ribbon::update(float randX, float randY){
	addParticle(randX, randY);
	drawCurve();
}

void Ribbon::draw(){
	//drawCurve();
}

void Ribbon::init()
{
	//particles = new RibbonParticle[_ribbonParticleAmount];
}

void Ribbon::addParticle(float randX, float randY)
{
	RibbonParticle tmp(randomness, this);

	if(particlesAssigned == ribbonParticleAmount)
	{
		for (int i = 1; i < ribbonParticleAmount; i++)
		{
			particles[i-1] = particles[i];
		}
		
		particles[ribbonParticleAmount - 1] = tmp;
		particles[ribbonParticleAmount - 1].px = randX;
		particles[ribbonParticleAmount - 1].py = randY;

		//cout << particles[ribbonParticleAmount - 1].py << "    " << randX << endl;

		return;
	}
	else
	{
		particles[particlesAssigned] = tmp;
		particles[particlesAssigned].px = randX;
		particles[particlesAssigned].py = randY;
		++particlesAssigned;
	}
	if (particlesAssigned > ribbonParticleAmount) ++particlesAssigned;
}

void Ribbon::drawCurve()
{	
	//smooth();
	for (int i = 1; i < particlesAssigned - 1; i++)
	{
		RibbonParticle &p = particles[i];
		p.calculateParticles(particles[i-1], particles[i+1], ribbonParticleAmount, i);
		//cout << particles[i].leftPX<< endl;
	}

	//fill(30);
	for (int i = particlesAssigned - 3; i > 1 - 1; i--)
	{
		//RibbonParticle p = particles[i];
		//RibbonParticle pm1 = particles[i-1];
				
		ofSetColor(ribbonColor);
		ofFill();

		//cout << particles[i].lcx2 << endl;

		if (i < particlesAssigned-3) 
		{	
			//ofnoStroke();
			ofBeginShape();			
			ofVertex(particles[i].lcx2, particles[i].lcy2);
			ofBezierVertex(particles[i].leftPX, particles[i].leftPY, particles[i-1].lcx2, particles[i-1].lcy2, particles[i-1].lcx2, particles[i-1].lcy2);
			ofVertex(particles[i-1].rcx2, particles[i-1].rcy2);
			ofBezierVertex(particles[i].rightPX, particles[i].rightPY, particles[i].rcx2, particles[i].rcy2, particles[i].rcx2, particles[i].rcy2);
			ofVertex(particles[i].lcx2, particles[i].lcy2);
			ofEndShape();
		}
	}
}