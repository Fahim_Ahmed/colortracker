#pragma once

#include "ofMain.h"
#include "configs.h"
#include "RibbonParticle.h"

//class RibbonParticle;

class Ribbon : public ofBaseApp {
	
public:
	int ribbonAmount;
	float randomness;
	int ribbonParticleAmount;         // length of the Particle Array (max number of points)
	int particlesAssigned;        // current amount of particles currently in the Particle array                                
	float radiusMax;              // maximum width of ribbon
	float radiusDivide;          // distance between current and next point / this = radius for first half of the ribbon
	float gravity;              // gravity applied to each particle
	float friction;             // friction applied to the gravity of each particle
	int maxDistance;             // if the distance between particles is larger than this the drag comes into effect
	float drag;                   // if distance goes above maxDistance - the points begin to grag. high numbers = less drag
	float dragFlare;           // degree to which the drag makes the ribbon flare out
	RibbonParticle particles[RIBBON_PARTICLE_AMOUNT];       // particle array
	ofColor ribbonColor;

	void setup();
	//void update(float randX, float randY);
	void draw();

	Ribbon();
	Ribbon(int _ribbonParticleAmount, ofColor _ribbonColor, float _randomness);
	void init();
	void update(float randX, float randY);
	void addParticle(float randX, float randY);
	void drawCurve();	
};