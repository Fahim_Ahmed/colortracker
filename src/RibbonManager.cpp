#include "RibbonManager.h"

RibbonManager::RibbonManager(){
	
}

RibbonManager::RibbonManager(int _ribbonAmount, int _ribbonParticleAmount, float _randomness, string _imgName)
{
	ribbonAmount = _ribbonAmount;
	ribbonParticleAmount = _ribbonParticleAmount;
	randomness = _randomness;
	imgName = _imgName;
	init();
}

void RibbonManager::setup(){
	
}

void RibbonManager::draw(){

}

void RibbonManager::init()
{
	img.loadImage(imgName);
	addRibbon();
}

void RibbonManager::addRibbon()
{
	//ribbons = new Ribbon[ribbonAmount];
	for (int i = 0; i < ribbonAmount; i++)
	{
		int xpos = int(ofRandom(img.width));
		int ypos = int(ofRandom(img.height));
		ofColor ribbonColor = img.getColor(xpos, ypos);
		//ofColor ribbonColor = 0xFFFFFF;

		Ribbon tmp(ribbonParticleAmount, ribbonColor, randomness);
		ribbons[i] = tmp;
	}
	//cout << ribbons[0].maxDistance << endl;
}

void RibbonManager::update(int currX, int currY) 
{
	for (int i = 0; i < ribbonAmount; i++)
	{
		//float randX = currX + (randomness / 2) - random(randomness);
		//float randY = currY + (randomness / 2) - random(randomness);

		float randX = currX;
		float randY = currY;

		ribbons[i].update(randX, randY);
	}
}

void RibbonManager::setRadiusMax(float value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].radiusMax = value; } }
void RibbonManager::setRadiusDivide(float value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].radiusDivide = value; } }
void RibbonManager::setGravity(float value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].gravity = value; } }
void RibbonManager::setFriction(float value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].friction = value; } }
void RibbonManager::setMaxDistance(int value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].maxDistance = value; } }
void RibbonManager::setDrag(float value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].drag = value; } }
void RibbonManager::setDragFlare(float value) { for (int i = 0; i < ribbonAmount; i++) { ribbons[i].dragFlare = value; } }

void RibbonManager::drawSome(){
	//cout << "aaaaaaaaaa" << endl;
	ofSetColor(135);
	ofCircle(100,100,150);
}