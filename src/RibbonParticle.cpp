#include "RibbonParticle.h"
#include "Ribbon.h"

RibbonParticle::RibbonParticle(){}

RibbonParticle::RibbonParticle(float rand, Ribbon *r){
	randomness = rand;
	ribbon = r;
	radius = 0;

	leftPX = 0, leftPY = 0, rightPX = 0, rightPY = 0;
	lpx = 0, lpy = 0, rpx = 0, rpy = 0;
	lcx1 = 0, lcy1 = 0, lcx2 = 0, lcy2 = 0;
	rcx1 = 0, rcy1 = 0, rcx2 = 0, rcy2 = 0;   

	xSpeed = 0;
	ySpeed = 0;
}

void RibbonParticle::setup(){
	
}

void RibbonParticle::update(){
	
}

void RibbonParticle::draw(){

}

void RibbonParticle::calculateParticles(const RibbonParticle& pMinus1, const RibbonParticle& pPlus1, int particleMax, int i)
{
	float div = 2;
	cx1 = (pMinus1.px + px) / div;
	cy1 = (pMinus1.py + py) / div;
	cx2 = (pPlus1.px + px) / div;
	cy2 = (pPlus1.py + py) / div;
	
	//cout << cx1 << endl;

	// calculate radians (direction of next point)
	float dx = cx2 - cx1;
	float dy = cy2 - cy1;
	 
	float pRadians = atan2(dy, dx);
	
	float distance = sqrt(dx*dx + dy*dy);

	if (distance > ribbon->maxDistance)   //  && i > 1 
	{
		float oldX = px;
		float oldY = py;
		px = px + ((ribbon->maxDistance/ribbon->drag) * cos(pRadians));
		py = py + ((ribbon->maxDistance/ribbon->drag) * sin(pRadians));
		xSpeed += (px - oldX) * ribbon->dragFlare;
		ySpeed += (py - oldY) * ribbon->dragFlare;

		//cout << "1. " << px << endl;
	}
		

	ySpeed += ribbon->gravity;
	xSpeed *= ribbon->friction;
	ySpeed *= ribbon->friction;	
	
	px += (xSpeed + ofRandom(0.3));
	py += (ySpeed + ofRandom(0.3));

	//cout << "2. " << radius << endl;

	float randX = ((randomness / 2) - ofRandom(randomness)) * distance;
	float randY = ((randomness / 2) - ofRandom(randomness)) * distance;
	px += randX;
	py += randY;


	//float radius = distance / 2;
	//if (radius > radiusMax) radius = ribbon.radiusMax;

	if (i > particleMax / 2) 
	{
		radius = distance / ribbon->radiusDivide;
	} 
	else 
	{
		radius = pPlus1.radius * 0.9;
	}

	if (radius > ribbon->radiusMax) radius = ribbon->radiusMax;
	if (i == particleMax - 2 || i == 1) 
	{
		if (radius > 1) radius = 1;
	}
	
	//cout << radius << endl;

	// calculate the positions of the particles relating to thickness
	leftPX = px + cos(pRadians + (HALF_PI * 3)) * radius;
	leftPY = py + sin(pRadians + (HALF_PI * 3)) * radius;
	rightPX = px + cos(pRadians + HALF_PI) * radius;
	rightPY = py + sin(pRadians + HALF_PI) * radius;

	// left and right points of current particle
	lpx = (pMinus1.lpx + lpx) / div;
	lpy = (pMinus1.lpy + lpy) / div;
	rpx = (pPlus1.rpx + rpx) / div;
	rpy = (pPlus1.rpy + rpy) / div;

	// left and right points of previous particle
	lcx1 = (pMinus1.leftPX + leftPX) / div;
	lcy1 = (pMinus1.leftPY + leftPY) / div;
	rcx1 = (pMinus1.rightPX + rightPX) / div;
	rcy1 = (pMinus1.rightPY + rightPY) / div;

	// left and right points of next particle
	lcx2 = (pPlus1.leftPX + leftPX) / div;
	lcy2 = (pPlus1.leftPY + leftPY) / div;
	rcx2 = (pPlus1.rightPX + rightPX) / div;
	rcy2 = (pPlus1.rightPY + rightPY) / div;

	//cout << pMinus1.leftPX << endl;
}