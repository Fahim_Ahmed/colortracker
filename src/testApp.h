#pragma once

#include "ofMain.h"
#include "config.h"
#include "RibbonManager.h"
#include "ofxOpenCv.h"


class testApp : public ofBaseApp{

	public:

		bool TESTING;
	//	int ribbonAmount;
		//int ribbonParticleAmount;
		float randomness;
		RibbonManager ribbonManager;
		ofVideoGrabber vFeed;

		ofxCvColorImage rgb, hsb;
		ofxCvGrayscaleImage filtered, red, green, blue;
		ofxCvContourFinder contours;
		int w,h,findHue;

		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
