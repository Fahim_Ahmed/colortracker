#include "testApp.h"

int mX, mY;

//--------------------------------------------------------------
void testApp::setup(){
	ofSetFrameRate(30);	
	ofBackground(0);
	//ofBackground(255, 146, 0);
	ofEnableSmoothing();
	
	//ofEnableAlphaBlending();
	//ofSetBackgroundAuto(false);

	TESTING = false;	
	randomness = .2;

	RibbonManager tmp(NUM_RIBBON, RIBBON_PARTICLE_AMOUNT, randomness, "rothko_01.jpg");
	ribbonManager = tmp;
	ribbonManager.setRadiusMax(24);                 // default = 8
	ribbonManager.setRadiusDivide(10);              // default = 10
	ribbonManager.setGravity(.07);                   // default = .03
	ribbonManager.setFriction(1.1);                  // default = 1.1
	ribbonManager.setMaxDistance(40);               // default = 40
	ribbonManager.setDrag(2.5);                      // default = 2
	ribbonManager.setDragFlare(0.015);                 // default = .008

	w = 320;
	h = 240;

	//vFeed.setVerbose(true);
	vFeed.initGrabber(w, h, true);

	rgb.allocate(w, h);	
	hsb.allocate(w, h);
	filtered.allocate(w, h);
	red.allocate(w, h);
	green.allocate(w, h);
	blue.allocate(w, h);
}

//--------------------------------------------------------------
void testApp::update(){
	//cout << mX <<endl;	
	/*
	vFeed.update();
	
	if (vFeed.isFrameNew()) {
		
		rgb.setFromPixels(vFeed.getPixels(), w, h);

		//mirror horizontal
		rgb.mirror(false, true);
		
		hsb = rgb;			
		hsb.convertRgbToHsv();				
		hsb.convertToGrayscalePlanarImages(red, green, blue);
		
		red.mirror(false, true);

		for (int i=0; i<w*h; i++) {
			//filtered.getPixels()[i] = ofInRange(red.getPixels()[i],findHue-5,findHue+5) ? 255 : 0;
		}
		filtered.flagImageChanged();
		contours.findContours(filtered, 50, w*h/2, 1, false);
	}*/

}

//--------------------------------------------------------------
void testApp::draw(){
//	vFeed.draw(0,0,1280,720);

	//ofSetColor(255);			
//	vFeed.draw(0,0);
	//hsb.draw(640,0);
	//red.draw(0,0);
	//green.draw(320,240);
	//blue.draw(0	,0);
	//filtered.draw(0,0);
	//contours.draw(0,0);

	

	//draw red circles for found blobs
/*	for (int i=0; i<contours.nBlobs; i++) {
		ofCircle(contours.blobs[i].centroid.x, contours.blobs[i].centroid.y, 10);
	//	ribbonManager.update(contours.blobs[i].centroid.x, contours.blobs[i].centroid.y);
	}  */
	//vFeed.draw(640,800);
	ribbonManager.update(mX, mY);	
	//ofSetColor(0,0);
	//ofFill();

	ofDrawBitmapString(ofToString(ofGetFrameRate())+"fps", 10, 15);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
	mX = x;
	mY = y;
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
	int mx = x % w;
	int my = y % h;

	//get hue value on mouse position
	findHue = red.getPixels()[my*w+mx];
	//cout << findHue <<endl;
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}