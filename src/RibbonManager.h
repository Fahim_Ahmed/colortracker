#pragma once

#include "ofMain.h"
#include "Ribbon.h"
#include "configs.h"

class RibbonManager : public ofBaseApp{
		
public:
	ofImage img;
	int ribbonAmount;
	int ribbonParticleAmount;
	float randomness;
	string imgName;
	Ribbon ribbons[NUM_RIBBON];

	void setup();
	void update(int currX, int currY);
	void draw();	

	RibbonManager();
	RibbonManager(int _ribbonAmount, int _ribbonParticleAmount, float _randomness, string _imgName);
	void init();
	void addRibbon();
	void setRadiusMax(float value);
	void setRadiusDivide(float value);
	void setGravity(float value);
	void setFriction(float value);
	void setMaxDistance(int value);
	void setDrag(float value);
	void setDragFlare(float vaue);
	void drawSome();
};